Name:      terraform-inventory
Version:   0.8
Release:   1%{?dist}
Summary:   Go utility to transform a Terraform State file into an Ansible Dynamic Inventory
Group:     Applications/System
License:   MIT
URL:       https://github.com/adammck/terraform-inventory/
Source0:   https://github.com/adammck/%{name}/releases/download/v%{version}/%{name}_v%{version}_linux_amd64.zip
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
This is a little Go app which generates a dynamic Ansible inventory from a Terraform state file. It allows one to spawn a bunch of instances with Terraform, then (re-)provision them with Ansible.

%prep
%setup -q -c

%install
mkdir -p %{buildroot}/%{_bindir}
cp %{name}* %{buildroot}/%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%attr(755, root, root) %{_bindir}/%{name}*

%doc

%changelog
* Thu May 30 2019 Alexander Bauer <sasha@linux.com>
- Write version for 0.8 format
