.PHONY: rpm sources

rpm: terraform-inventory.spec sources
	rpmbuild -bb terraform-inventory.spec --define "_topdir $(PWD)"

sources: terraform-inventory.spec
	mkdir -p SOURCES/
	spectool -g -C "SOURCES/" terraform-inventory.spec
